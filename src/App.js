import React, { Component } from "react";
import { Switch, Route, Link, BrowserRouter as Router } from "react-router-dom";
import Login from "./components/Login";
import Privacy from "./components/privacy";
import ProductList from "./components/ProductList";
import AddProduct from "./components/AddProduct";
import SearchForm from "./components/SearchForm";
import BookingReview from "./components/BookingReview";
import Cart from "./components/Cart";
import data from "./Data";
import Context from "./Context";
import axios from 'axios';
import FacebookLogin from 'react-facebook-login';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flightData: {},
      depart: new Date(),
      return: new Date(),
      fromcity: '',
      tocity: '',
      fclass:'',
      adult: 1,
      child: 0,
      userDetails: null,
      cart: {},
      products: [],
      searchTerm: '',
      active: 'rt',
      fromerror: '',
      toerror: ''
    };
    this.handleChange = this.handleChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    
    this.routerRef = React.createRef();
  }
  handleChange(event) {
     let nam = event.target.name;
    let val = event.target.value;
    this.setState({[nam]: val});
  }

  responseFacebook(response) {
    console.log(response);
    if (response) {
      let userDetails = {
      "name": response.name,
      "email": response.email,
      "picture": response.picture.data.profilePic,
      "userID": response.userID
      }
      this.setState({ userDetails });
      localStorage.setItem("user", JSON.stringify(userDetails));
      return true;
    }
    return false;
  }

  onFormSubmit(e) {
    let flightData = {
      depart: this.state.depart,
      return: this.state.return,
      fromcity: this.state.fromcity,
      tocity: this.state.tocity,
      fclass:this.state.fclass,
      adult: this.state.adult,
      child: this.state.child
    }
    console.log('flight data=>', flightData);
    e.preventDefault();
     // axios.get('http://localhost:9090/seat/',flightData)
    // .then(response =>  {    
    //     //////////////problem here with setState (doesn't set state)
    //                    this.setState({seats:response.data})
    //                    if(response.data){
    //                    console.log("11111111111111")
    //                    }

    //                    } );
    console.log(this.state.startDate)
    if (!this.state.fromcity || this.state.fromcity === '' || this.state.fromcity === 'undefined') {
      this.setState({ fromerror: "Fill From Fields!" });
    } else if (!this.state.tocity || this.state.tocity === '' || this.state.tocity === 'undefined') {
      this.setState({ toerror: "Fill To Fields!" });
    } else {
      data.flightData = 
      {
        toCity: this.state.tocity,
        fromCity: this.state.fromcity,
      };
      this.setState({ products:data.initProducts});
      localStorage.setItem("flightData", JSON.stringify(flightData));
      this.routerRef.current.history.push("/flightlist");
    }
  }

  setFilter = (name) => {
    console.log('filter set', name);
    this.setState({ active: name })
  };

  editSearchTerm = (e) => {
    if (e.target.value) {
      this.setState({searchTerm: e.target.value});
      this.dynamicSearch(e.target.value);
    } else {
      this.setState({searchTerm: ''});
      this.setState({ products:data.initProducts});
    }
  }
  login = (usn, pwd) => {
    let user = data.users.find(u => u.username === usn && u.password === pwd);
    if (user) {
      let userDetails = {
        "name": user.name,
        "email": user.email,
        "picture": user.picture.data.profilePic,
        "userID": user.userID
      }
      this.setState({ userDetails });
      localStorage.setItem("user", JSON.stringify(userDetails));
      return true;
    }
    return false;
  };

  logout = e => {
    e.preventDefault();
    this.setState({ userDetails: null });
    localStorage.removeItem("user");
  };
 
  addProduct = (product, callback) => {
    let products = this.state.products.slice();
    products.push(product);
    localStorage.setItem("products", JSON.stringify(products));
    this.setState({ products }, () => callback && callback());
  };

  flightSelection = selectedFlight => {
    console.log('selected flight', selectedFlight);
    this.setState({ flightData:selectedFlight });   
  let flightData = JSON.parse(localStorage.getItem("flightData"));
  flightData.type = selectedFlight.type;
  flightData.price = selectedFlight.product.price;
    localStorage.setItem("flightData", JSON.stringify(flightData));
    this.routerRef.current.history.push("/bookingreview");
  };

  checkout = () => {
    if (!this.state.userDetails) {
      this.routerRef.current.history.push("/login");
      return;
    }
    const cart = this.state.cart;
    const products = this.state.products.map(p => {
      if (cart[p.name]) {
        p.stock = p.stock - cart[p.name].amount;
      }
      return p;
    });
    this.setState({ products });
    this.clearCart();
  };

  removeFromCart = cartItemId => {
    let cart = this.state.cart;
    delete cart[cartItemId];
    localStorage.setItem("cart", JSON.stringify(cart));
    this.setState({ cart });
  };

  clearCart = () => {
    let cart = {};
    localStorage.setItem("cart", JSON.stringify(cart));
    this.setState({ cart });
  };

  componentDidMount() {
    let cart = localStorage.getItem("cart");
    let user = localStorage.getItem("user");
    cart = cart ? JSON.parse(cart) : {};
    user = user ? JSON.parse(user) : null;
    this.setState({user, cart });
  }

  render() {
    return (
      <Context.Provider
        value={{
          ...this.state,
          removeFromCart: this.removeFromCart,
          flightSelection: this.flightSelection,
          login: this.login,
          addProduct: this.addProduct,
          clearCart: this.clearCart,
          checkout: this.checkout,
          editSearchTerm: this.editSearchTerm,
          searchTerm: this.state.searchTerm,
          flightSearch: this.flightSearch,
          startDate: this.state.startDate,
          flightData: this.state.flightData,
          onFormSubmit: this.onFormSubmit,
          handleChange: this.handleChange,
          active: this.state.active,
          setFilter: this.setFilter,
          fromerror: this.state.fromerror,
          toerror: this.state.toerror
        }}
      >
        <Router ref={this.routerRef}>
        <div className="App">
        <nav
              className="navbar container"
              role="navigation"
              aria-label="main navigation"
            >
              <div className="navbar-brand">
                <b className="navbar-item is-size-4 "><Link to="/">Flight-Booking</Link></b>

                <a
                  href="/"
                  role="button"
                  className="navbar-burger burger"
                  aria-label="menu"
                  aria-expanded="false"
                  data-target="navbarBasicExample"
                  onClick={e => {
                    e.preventDefault();
                    this.setState({ showMenu: !this.state.showMenu });
                  }}
                >
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                  <span aria-hidden="true"></span>
                </a>
              </div>
              <div
                className={`navbar-menu ${
                  this.state.showMenu ? "is-active" : ""
                } justify-content-end align-items-center mr-md-5`}
              >
                <Link to="/privacy" className="navbar-item btn btn-link">
                   Privacy Policy 
                </Link>
                {!this.state.userDetails ? (                 
                <FacebookLogin
                appId="4455488691187735"
                autoLoad={true}
                fields="name,email,picture"
                scope="public_profile,email,user_posts,user_photos,user_likes"
                callback={this.responseFacebook}
              />
                ) : (
                  <div>
                  </div>
                )}
                {!this.state.userDetails ? (
                  <Link to="/login" className="navbar-item btn btn-outline-info action-button">
                    Login
                  </Link>
                ) : (
                  <div><div className="username">Hi {this.state.userDetails.name}</div>
                  <Link to="/" className="navbar-item float-left" onClick={this.logout}>
                    Logout
                  </Link>
                  <img src={this.state.userDetails.picture} alt="" className="profileimg"></img>
                  </div>
                )}
              </div>
            </nav>
           
            <div id="booking" className="section">
              <div className="section-center">
                    
                <Switch>    
                  <Route exact path="/" component={SearchForm} />
                  {/* <Route exact path="/" component={ProductList} /> */}
                  <Route exact path="/login" component={Login} />
                  <Route exact path="/privacy" component={Privacy} />
                  <Route exact path="/cart" component={Cart} />
                  <Route exact path="/add-product" component={AddProduct} />
                  <Route exact path="/flightlist" component={ProductList} />
                  <Route exact path="/bookingreview" component={BookingReview} />
                </Switch>
              </div>
          </div>
          </div>
        </Router>
      </Context.Provider>
    );
  }
}
